package com.francislagueu.bankaccount.services.impl;

import com.francislagueu.bankaccount.commands.CreateAccountCommand;
import com.francislagueu.bankaccount.commands.CreditMoneyCommand;
import com.francislagueu.bankaccount.commands.DebitMoneyCommand;
import com.francislagueu.bankaccount.dto.AccountCreateDTO;
import com.francislagueu.bankaccount.dto.MoneyCreditDTO;
import com.francislagueu.bankaccount.dto.MoneyDebitDTO;
import com.francislagueu.bankaccount.services.AccountCommandService;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class AccountCommandServiceImpl implements AccountCommandService {

    private final CommandGateway commandGateway;

    public AccountCommandServiceImpl(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public CompletableFuture<String> createAccount(AccountCreateDTO account) {
        return commandGateway.send(new CreateAccountCommand(UUID.randomUUID().toString(), account.getStartingBalance(), account.getCurrency()));
    }

    @Override
    public CompletableFuture<String> creditMoneyToAccount(String accountNumber, MoneyCreditDTO credit) {
        return commandGateway.send(new CreditMoneyCommand(accountNumber, credit.getCreditAmount(), credit.getCurrency()));
    }

    @Override
    public CompletableFuture<String> debitMoneyFromAccount(String accountNumber, MoneyDebitDTO debit) {
        return commandGateway.send(new DebitMoneyCommand(accountNumber, debit.getDebitAmount(), debit.getCurrency()));
    }
}
