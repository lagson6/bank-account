package com.francislagueu.bankaccount.services;

import com.francislagueu.bankaccount.dto.AccountCreateDTO;
import com.francislagueu.bankaccount.dto.MoneyCreditDTO;
import com.francislagueu.bankaccount.dto.MoneyDebitDTO;

import java.util.concurrent.CompletableFuture;

public interface AccountCommandService {
    CompletableFuture<String> createAccount(AccountCreateDTO account);
    CompletableFuture<String> creditMoneyToAccount(String accountNumber, MoneyCreditDTO credit);
    CompletableFuture<String> debitMoneyFromAccount(String accountNumber, MoneyDebitDTO debit);
}


