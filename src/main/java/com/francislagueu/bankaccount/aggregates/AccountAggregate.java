package com.francislagueu.bankaccount.aggregates;

import com.francislagueu.bankaccount.commands.CreateAccountCommand;
import com.francislagueu.bankaccount.commands.CreditMoneyCommand;
import com.francislagueu.bankaccount.commands.DebitMoneyCommand;
import com.francislagueu.bankaccount.events.*;
import com.francislagueu.bankaccount.utils.Status;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.commandhandling.model.AggregateLifecycle;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class AccountAggregate {
    @AggregateIdentifier
    private String id;

    private double accountBalance;
    private String currency;
    private String status;

    public AccountAggregate() {
    }

    @CommandHandler
    public AccountAggregate(CreateAccountCommand cmd){
        AggregateLifecycle.apply(new AccountCreatedEvent(cmd.id, cmd.accountBalance, cmd.currency));
    }

    @EventSourcingHandler
    protected void on(AccountCreatedEvent evt){
        this.id = evt.id;
        this.accountBalance = evt.accountBalance;
        this.currency = evt.currency;
        this.status = String.valueOf(Status.CREATED);
        AggregateLifecycle.apply(new AccountActivatedEvent(this.id, Status.ACTIVATED));
    }

    @EventSourcingHandler
    protected void on(AccountActivatedEvent evt){
        this.status = String.valueOf(evt.status);
    }

    @CommandHandler
    protected void on(CreditMoneyCommand cmd){
        AggregateLifecycle.apply(new MoneyCreditedEvent(cmd.id, cmd.creditAmount, cmd.currency));
    }

    @EventSourcingHandler
    protected void on(MoneyCreditedEvent evt){
        if(this.accountBalance < 0 & (this.accountBalance + evt.creditAmount) >= 0){
            AggregateLifecycle.apply(new AccountActivatedEvent(this.id, Status.ACTIVATED));
        }
        this.accountBalance += evt.creditAmount;
    }

    @CommandHandler
    protected void on(DebitMoneyCommand cmd){
        AggregateLifecycle.apply(new MoneyDebitedEvent(cmd.id, cmd.debitAmount, cmd.currency));
    }

    @EventSourcingHandler
    protected void on(MoneyDebitedEvent evt){
        if(this.accountBalance >= 0 & (this.accountBalance - evt.debitAmount) < 0){
            AggregateLifecycle.apply(new AccountHeldEvent(this.id, Status.HOLD));
        }
        this.accountBalance -= evt.debitAmount;
    }

    @EventSourcingHandler
    protected void on(AccountHeldEvent evt){
        this.status = String.valueOf(evt.status);
    }

}
