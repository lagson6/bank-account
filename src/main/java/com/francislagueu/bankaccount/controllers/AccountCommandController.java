package com.francislagueu.bankaccount.controllers;

import com.francislagueu.bankaccount.dto.AccountCreateDTO;
import com.francislagueu.bankaccount.dto.MoneyCreditDTO;
import com.francislagueu.bankaccount.dto.MoneyDebitDTO;
import com.francislagueu.bankaccount.services.AccountCommandService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/bank-accounts")
@Api(value = "Accounts Commands", description = "Account Commands Related Endpoints", tags = "Account Commands")
public class AccountCommandController {

    private final AccountCommandService accountCommandService;

    public AccountCommandController(AccountCommandService accountCommandService) {
        this.accountCommandService = accountCommandService;
    }

    @PostMapping
    public CompletableFuture<String> createAccount(@RequestBody AccountCreateDTO accountCreateDTO){
        return accountCommandService.createAccount(accountCreateDTO);
    }

    @PutMapping(value = "/credits/{accountNumber}")
    public CompletableFuture<String> creditMoneyToAccount(@PathVariable(value = "accountNumber") String accountNumber, @RequestBody MoneyCreditDTO creditDTO){
        return accountCommandService.creditMoneyToAccount(accountNumber, creditDTO);
    }

    @PutMapping(value = "/debits/{accountNumber}")
    public CompletableFuture<String> debitMoneyFromAccount(@PathVariable(value = "accountNumber") String accountNumber, @RequestBody MoneyDebitDTO debitDTO){
        return accountCommandService.debitMoneyFromAccount(accountNumber, debitDTO);
    }
}
